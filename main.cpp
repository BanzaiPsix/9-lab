#include <SFML/Graphics.hpp>
#include <iostream>
#include <thread>
#include <chrono>

using namespace std::chrono_literals;;

int main()
{
    sf::RenderWindow window(sf::VideoMode(1000, 800), L"9 Лабораторная работа");

    // круги 
    sf::CircleShape shape1(30.f);
    float shape1_x = 1000, shape1_y = 100;
    shape1.setFillColor(sf::Color::Blue);
    shape1.setPosition(shape1_x, shape1_y);

    sf::CircleShape shape2(10.f);
    float shape2_x = 1000, shape2_y = 240;
    shape2.setFillColor(sf::Color::Green);
    shape2.setPosition(shape2_x, shape2_y);

    sf::CircleShape shape3(150.f);
    float shape3_x = 1000, shape3_y = 380;
    shape3.setFillColor(sf::Color::Red);
    shape3.setPosition(shape3_x, shape3_y);

    // Цикл работает до тех пор, пока окно открыто
    while (window.isOpen())
    {
        // Переменная для хранения события
        sf::Event event;
        // Цикл по всем событиям
        while (window.pollEvent(event))
        {
            // Обработка событий
            // Если нажат крестик, то
            if (event.type == sf::Event::Closed)
                // окно закрывается
                window.close();
        }
        //move
        shape1_x--;
        if (shape1_x <= 0)
        {
            shape1_x = 0;
        }


        shape2_x = shape2_x - 2;
        if (shape2_x <= 0)
        {
            shape2_x = 0;
        }

        shape3_x = shape3_x - 3;
        if (shape3_x <= 0)
        {
            shape3_x = 0;
        }
        window.clear();
        //Gradus++;
        //shape3.setRotation(Gradus);

        shape1.setPosition(shape1_x, shape1_y);
        shape2.setPosition(shape2_x, shape2_y);
        shape3.setPosition(shape3_x, shape3_y);
        
        

        
        window.draw(shape1);
        window.draw(shape2);
        window.draw(shape3);
        
        window.display();

        std::this_thread::sleep_for(4ms);
    }
    return 0;
}
